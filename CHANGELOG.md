# CHANGELOG

## 1.3.1 - 2024-04-18

Changed not to convert in preview even in issues.
In 1.3.0 it was not converted only in Wiki.
Changed to not convert in other issues and project settings as well.

## 1.3.0 - 2024-04-15

Changed to not convert in preview when `redmine_wysiwyg_editor` plugin is present.
Display problems in the visual editor will be improved.

## 1.2.0 - 2024-03-29

Make it easy to see that it could be copied.
Click on the "Copy Path" link to get the "Copied!" popup.

## 1.1.0 - 2024-03-15

Improve display text

Separated by whitespace, but still easy to copy:

INPUT `\\path\file name`

`\\path\file (Copy path) name` ->  
`(Copy path) \\path\file name`

## 1.0.0 - 2024-01-24

Initial release!!!
