namespace :unc do
  desc "Tag"
  task :tag => :environment do
    plugin = Redmine::Plugin.find(:unc)
    version = plugin.version
    cd(plugin.directory) do
      sh("git", "tag",
         "-a", "v#{version}",
         "-m", "#{version} has been released!!!")
      sh("git", "push", "--tags")
    end
  end
end
