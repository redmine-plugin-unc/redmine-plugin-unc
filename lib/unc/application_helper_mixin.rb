# Copyright (C) 2024  Abe Tomoaki <abe@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

module Unc
  module ApplicationHelperMixin
    def copied_popup_js
      <<~JS.delete("\n")
      $('<span>')
        .css('position', 'absolute')
        .insertBefore(this)
        .text(#{l(:text_unc_path_copied).to_json})
        .addClass('ui-state-default ui-priority-primary')
        .delay(750)
        .hide(function() {
          $(this).remove();
        });
      JS
    end

    def link_unc_path(path)
      link_to_function(
        "(#{l(:text_unc_path_copy)}) #{path}",
        "copyTextToClipboard(this); #{copied_popup_js}",
        class: 'icon icon-copy-link',
        data: {'clipboard-text' => path}
      )
    end

    def parse_unc_paths(text)
      text.gsub!(/"(\\\\.+?)"|\\\\[^\s<]+/) do |matched|
        link_unc_path($1 || matched)
      end
    end

    def preview?
      ## If we don't have associated input, it must not be preview.
      # Mailer doesn't have controller.request.
      return false unless controller.respond_to?(:request)
      # For example, this happens with background full text search
      # update by
      # https://github.com/clear-code/redmine_full_text_search/ .
      return false if controller.request.get_header("rack.input").nil?

      return true if params && params['action'] == 'preview'
      return true if controller.controller_path == 'previews'
      false
    end

    def have_plugin?(name)
      begin
        Redmine::Plugin.find(name)
      rescue Redmine::PluginNotFound
        false
      else
        true
      end
    end

    def convert_unc_path?
      # View hook doesn't have controller and view hook doesn't have
      # preview mode because it always renders its content.
      return false if controller.nil?
      return true unless preview?
      # When the display is broken by other plugin,
      # it is not converted.
      return false if have_plugin?(:redmine_wysiwyg_editor)
      true
    end

    def parse_non_pre_blocks(text, obj, macros, options={})
      super do |txt|
        yield txt
        parse_unc_paths(txt) if convert_unc_path?
      end
    end
  end
end

ApplicationHelper.prepend(Unc::ApplicationHelperMixin)
