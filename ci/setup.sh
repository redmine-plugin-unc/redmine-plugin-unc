#!/usr/bin/env bash

# Copyright (C) 2024  Abe Tomoaki <abe@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

set -eux

# Install the necessary packages for testing Redmine itself.
apt-get update
apt-get install -y gsfonts ghostscript
# Update ImageMagick policy to allow read PDF. For testing Redmine itself.
sed -i 's/domain="coder" rights="none" pattern="PDF"/domain="coder" rights="read" pattern="PDF"/' \
  /etc/ImageMagick-6/policy.xml

branch=${1:-5.1-stable}

tar \
  -cf plugin_unc.tar \
  -v \
  --exclude-vcs \
  --exclude-vcs-ignores \
  .
git clone \
  --depth 1 \
  --branch ${branch} \
  https://github.com/redmine/redmine.git \
  redmine
mkdir -p redmine/plugins/unc
tar \
  -xf plugin_unc.tar \
  -C redmine/plugins/unc
cat <<DATABASE_YML > redmine/config/database.yml
test:
  adapter: sqlite3
  database: db/redmine.test.sqlite3
DATABASE_YML
cd redmine
bundle install
bin/rails db:create
bin/rails generate_secret_token
bin/rails db:migrate
bin/rails redmine:plugins:migrate
REDMINE_LANG=en bin/rails redmine:load_default_data
cd -
