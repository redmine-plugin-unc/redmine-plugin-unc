# Copyright (C) 2024  Abe Tomoaki <abe@clear-code.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require File.expand_path('../../../test_helper', __FILE__)

module Unc
  class ApplicationHelperMixinTest < Redmine::HelperTest
    ON_CLICK_TEXT_EN = ERB::Util.h(<<~JS.delete("\n"))
    copyTextToClipboard(this);
     $('<span>')
      .css('position', 'absolute')
      .insertBefore(this)
      .text("Copied!")
      .addClass('ui-state-default ui-priority-primary')
      .delay(750)
      .hide(function() {
        $(this).remove();
      });;
     return false;
    JS
    ON_CLICK_TEXT_JA = ERB::Util.h(<<~JS.delete("\n"))
    copyTextToClipboard(this);
     $('<span>')
      .css('position', 'absolute')
      .insertBefore(this)
      .text("コピー完了！")
      .addClass('ui-state-default ui-priority-primary')
      .delay(750)
      .hide(function() {
        $(this).remove();
      });;
     return false;
    JS

    test 'copied_popup_js at en' do
      expected = <<~JS.delete("\n")
      $('<span>')
        .css('position', 'absolute')
        .insertBefore(this)
        .text("Copied!")
        .addClass('ui-state-default ui-priority-primary')
        .delay(750)
        .hide(function() {
          $(this).remove();
        });
      JS
      with_locale 'en' do
        assert_equal(expected, copied_popup_js)
      end
    end

    test 'copied_popup_js at ja' do
      expected = <<~JS.delete("\n")
      $('<span>')
        .css('position', 'absolute')
        .insertBefore(this)
        .text("コピー完了！")
        .addClass('ui-state-default ui-priority-primary')
        .delay(750)
        .hide(function() {
          $(this).remove();
        });
      JS
      with_locale 'ja' do
        assert_equal(expected, copied_popup_js)
      end
    end

    test 'link_unc_path at en' do
      expected = <<~"HTML".delete("\n")
      <a
       href="#"
       onclick="#{ON_CLICK_TEXT_EN}"
       class="icon icon-copy-link"
       data-clipboard-text="\\\\a\\b\\c">
      (Copy path) \\\\a\\b\\c
      </a>
      HTML
      with_locale 'en' do
        assert_equal(
          expected,
          link_unc_path('\\\\a\\b\\c')
        )
      end
    end

    test 'link_unc_path at ja' do
      expected = <<~"HTML".delete("\n")
      <a
       href="#"
       onclick="#{ON_CLICK_TEXT_JA}"
       class="icon icon-copy-link"
       data-clipboard-text="\\\\a\\b\\c">
      (パスをコピー) \\\\a\\b\\c
      </a>
      HTML
      with_locale 'ja' do
        assert_equal(
          expected,
          link_unc_path('\\\\a\\b\\c')
        )
      end
    end

    test 'parse_unc_paths: No path.' do
      text = '<p>foo</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end
      assert_equal('<p>foo</p>', text)
    end

    test 'parse_unc_paths: No double quotes. one path. <br /> at the end.' do
      text = "<p>\\\\a\\b\\c<br />\n"
      with_locale 'en' do
        parse_unc_paths(text)
      end

      expected = <<~HTML.delete("\n") + "\n"
      <p>
      <a
       href="#"
       onclick="#{ON_CLICK_TEXT_EN}"
       class="icon icon-copy-link"
       data-clipboard-text="\\\\a\\b\\c">
      (Copy path) \\\\a\\b\\c
      </a>
      <br />
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: No double quotes. one path. </p> at the end.' do
      text = '<p>\\\\a\\b\\c</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end

      expected = <<~"HTML".delete("\n")
      <p>
      <a
       href="#"
       onclick="#{ON_CLICK_TEXT_EN}"
       class="icon icon-copy-link"
       data-clipboard-text="\\\\a\\b\\c">
      (Copy path) \\\\a\\b\\c
      </a>
      </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: With double quotes. one path.' do
      text = '<p>"\\\\a\\b\\c"</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end

      expected = <<~"HTML".delete("\n")
      <p>
      <a
       href="#"
       onclick="#{ON_CLICK_TEXT_EN}"
       class="icon icon-copy-link"
       data-clipboard-text="\\\\a\\b\\c">
      (Copy path) \\\\a\\b\\c
      </a>
      </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: No double quotes. one path. With half-width spaces.' do
      text = '<p>\\\\a\\b b\\c</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end

      expected = <<~"HTML".delete("\n")
      <p>
      <a
       href="#"
       onclick="#{ON_CLICK_TEXT_EN}"
       class="icon icon-copy-link"
       data-clipboard-text="\\\\a\\b">
      (Copy path) \\\\a\\b
      </a>
       b\\c
      </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: With double quotes. one path. With half-width spaces.' do
      text = '<p>"\\\\a\\b b\\c"</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end

      expected = <<~"HTML".delete("\n")
      <p>
      <a
       href="#"
       onclick="#{ON_CLICK_TEXT_EN}"
       class="icon icon-copy-link"
       data-clipboard-text="\\\\a\\b b\\c">
      (Copy path) \\\\a\\b b\\c
      </a>
      </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: No double quotes. multiple paths. No half-width spaces.' do
      text = '<p>\\\\a\\b\\c,\\\\d\\e\\f</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end

      expected = <<~"HTML".delete("\n")
      <p>
      <a
       href="#"
       onclick="#{ON_CLICK_TEXT_EN}"
       class="icon icon-copy-link"
       data-clipboard-text="\\\\a\\b\\c,\\\\d\\e\\f">
      (Copy path) \\\\a\\b\\c,\\\\d\\e\\f
      </a>
      </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: With double quotes. multiple paths. No half-width spaces.' do
      text = '<p>"\\\\a\\b\\c","\\\\d\\e\\f"</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end
      expected = <<~"HTML".delete("\n")
        <p>
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\a\\b\\c">
        (Copy path) \\\\a\\b\\c
        </a>,
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\d\\e\\f">
        (Copy path) \\\\d\\e\\f
        </a>
        </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: No double quotes. multiple paths. With half-width spaces.' do
      text = '<p>\\\\a\\b\\c, \\\\d\\e\\f</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end
      expected = <<~"HTML".delete("\n")
        <p>
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\a\\b\\c,">
        (Copy path) \\\\a\\b\\c,
        </a> 
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\d\\e\\f">
        (Copy path) \\\\d\\e\\f
        </a>
        </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: With double quotes. multiple paths. With half-width spaces.' do
      text = '<p>"\\\\a\\b\\c","\\\\d\\e\\f"</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end
      expected = <<~"HTML".delete("\n")
        <p>
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\a\\b\\c">
        (Copy path) \\\\a\\b\\c
        </a>,
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\d\\e\\f">
        (Copy path) \\\\d\\e\\f
        </a>
        </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: No double quotes. multiple paths. Separate by <br />' do
      text = '<p>\\\\a\\b\\c<br />\\\\d\\e\\f</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end
      expected = <<~"HTML".delete("\n")
        <p>
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\a\\b\\c">
        (Copy path) \\\\a\\b\\c
        </a>
        <br />
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\d\\e\\f">
        (Copy path) \\\\d\\e\\f
        </a>
        </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: With double quotes. multiple paths. Separate by <br />' do
      text = '<p>"\\\\a\\b\\c"<br />"\\\\d\\e\\f"</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end
      expected = <<~"HTML".delete("\n")
        <p>
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\a\\b\\c">
        (Copy path) \\\\a\\b\\c
        </a>
        <br />
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\d\\e\\f">
        (Copy path) \\\\d\\e\\f
        </a>
        </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: Some paths in the text.' do
      text = '<p>The file is here: "\\\\a\\b b\\c"<br />Bug report here: \\\\d\\e\\f\\g\\bug.txt and \\\\h\\i\\bug.txt</p>'
      with_locale 'en' do
        parse_unc_paths(text)
      end
      expected = <<~"HTML".delete("\n")
        <p>The file is here: 
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\a\\b b\\c">
        (Copy path) \\\\a\\b b\\c
        </a>
        <br />Bug report here: 
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\d\\e\\f\\g\\bug.txt">
        (Copy path) \\\\d\\e\\f\\g\\bug.txt
        </a>
         and 
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_EN}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\h\\i\\bug.txt">
        (Copy path) \\\\h\\i\\bug.txt
        </a>
        </p>
      HTML
      assert_equal(expected, text)
    end

    test 'parse_unc_paths: Some paths in the text. With Multi-byte characters.' do
      text = '<p>資料: "\\\\a\\b b\\資料.xlsx"<br />バグのまとめ: \\\\d\\e\\プロジェクトA\\g\\不具合.txt and \\\\h\\プロジェクトB\\bug.txt</p>'
      with_locale 'ja' do
        parse_unc_paths(text)
      end
      expected = <<~HTML.delete("\n")
        <p>資料: 
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_JA}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\a\\b b\\資料.xlsx">
        (パスをコピー) \\\\a\\b b\\資料.xlsx
        </a>
        <br />バグのまとめ: 
        <a
         href="#" onclick="#{ON_CLICK_TEXT_JA}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\d\\e\\プロジェクトA\\g\\不具合.txt">
        (パスをコピー) \\\\d\\e\\プロジェクトA\\g\\不具合.txt
        </a>
         and 
        <a
         href="#"
         onclick="#{ON_CLICK_TEXT_JA}"
         class="icon icon-copy-link"
         data-clipboard-text="\\\\h\\プロジェクトB\\bug.txt">
        (パスをコピー) \\\\h\\プロジェクトB\\bug.txt
        </a>
        </p>
      HTML
      assert_equal(expected, text)
    end

    # `parse_non_pre_blocks` is called within `textilizable`.
    test 'parse_non_pre_blocks: With link. No UNC path.' do
      with_settings text_formatting: 'common_mark' do
        actual = textilizable('[test](foo)')
        assert_equal(
          '<p><a href="foo">test</a></p>',
          actual
        )
      end
    end

    test 'parse_non_pre_blocks: With link. With UNC path.' do
      with_settings text_formatting: 'common_mark' do
        with_locale 'en' do
          text = <<~TEXT
            [test](foo)
            "\\\\\\\\server\\\\path\\\\to\\\\file"
          TEXT
          actual = textilizable(text)
          expected = <<~"HTML".chomp
            <p><a href="foo">test</a><br>
            <a href="#" onclick="#{ON_CLICK_TEXT_EN}" class="icon icon-copy-link" data-clipboard-text="\\\\server\\path\\to\\file">(Copy path) \\\\server\\path\\to\\file</a></p>
          HTML
          assert_equal(expected, actual)
        end
      end
    end

    test 'parse_non_pre_blocks: UNC path in "```".'  do
      with_settings text_formatting: 'common_mark' do
        text = <<~TEXT
          ```
          \\\\\\\\server\\\\path\\\\to\\\\file
          ```
        TEXT
        actual = textilizable(text)
        expected = <<~HTML.chomp
          <pre><code>\\\\\\\\server\\\\path\\\\to\\\\file
          </code></pre>
        HTML
        assert_equal(expected, actual)
      end
    end

    test 'parse_non_pre_blocks: UNC path in "`".'  do
      with_settings text_formatting: 'common_mark' do
        actual = textilizable('`\\\\\\\\server\\\\path\\\\to\\\\file`')
        assert_equal(
          '<p><code>\\\\\\\\server\\\\path\\\\to\\\\file</code></p>',
          actual
        )
      end
    end

    test 'convert_unc_path?: in wiki preview.'  do
      params['action'] = 'preview'
      if have_plugin?(:redmine_wysiwyg_editor)
        assert(!convert_unc_path?)
      else
        assert(convert_unc_path?)
      end
    end

    test 'parse_non_pre_blocks: in wiki preview.'  do
      if have_plugin?(:redmine_wysiwyg_editor)
        expected = '<p>\\\\a\\b\\c</p>'
      else
        expected = <<~"HTML".delete("\n")
          <p>
          <a
           href="#"
           onclick="#{ON_CLICK_TEXT_EN}"
           class="icon icon-copy-link"
           data-clipboard-text="\\\\a\\b\\c">
          (Copy path) \\\\a\\b\\c
          </a>
          </p>
        HTML
      end

      with_settings text_formatting: 'common_mark' do
        params['action'] = 'preview'
        actual = textilizable('\\\\\\\\a\\b\\c')
        assert_equal(expected, actual)
      end
    end

    test 'convert_unc_path?: in preview using PreviewsController.'  do
      # PreviewsController is used in
      # project settings, issues, documents.

      controller.controller_path = "previews"
      if have_plugin?(:redmine_wysiwyg_editor)
        assert(!convert_unc_path?)
      else
        assert(convert_unc_path?)
      end
    end

    test 'parse_non_pre_blocks: in preview using PreviewsController.'  do
      if have_plugin?(:redmine_wysiwyg_editor)
        expected = '<p>\\\\a\\b\\c</p>'
      else
        expected = <<~"HTML".delete("\n")
          <p>
          <a
           href="#"
           onclick="#{ON_CLICK_TEXT_EN}"
           class="icon icon-copy-link"
           data-clipboard-text="\\\\a\\b\\c">
          (Copy path) \\\\a\\b\\c
          </a>
          </p>
        HTML
      end

      with_settings text_formatting: 'common_mark' do
        controller.controller_path = "previews"
        actual = textilizable('\\\\\\\\a\\b\\c')
        assert_equal(expected, actual)
      end
    end
  end
end
