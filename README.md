# Redmine plugin UNC

This plugin can easily access to UNC-format paths.

You can copy to the clipboard with a click.

![](exmaple.png)

## Install

Install this plugin:

```bash
cd redmine
git clone https://gitlab.com/redmine-plugin-unc/redmine-plugin-unc.git plugins/unc
```

Restart Redmine.

## Uninstall

Uninstall this plugin:

```bash
cd redmine
rm -rf plugins/unc
```

Restart Redmine.

## For developers

### Release a new version

1. (optional) If there are many additional features, the version number will be updated in `init.rb`
    * Not required for patch release
2. Add an entry for new release to `CHANGELOG.md`
3. Run `bin/rails unc:tag`
    * Run the task in directory of Redmine itself, not in directory of this plugin
    * This task adds a Git tag and push
4. Increment the patch version in `init.rb`

## License

GPLv2 or later. See [LICENSE](LICENSE) for details.
